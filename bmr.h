#pragma once

enum bmr_sex {
	BMR_SEX_F,
	BMR_SEX_M,
};

#define $bmr_get_double(_res, str, e) 														\
	({ 																					\
		char *end; 																		\
		errno = 0; 																		\
		*_res = strtod (str, &end); 														\
		/* If you've provided e, it's an error if you strtof doesn't parse */ 			\
		/* to e. If not, it's an error if you didn't parse anything */ 					\
		errno || (e? end != e: end == str)? 1: 0; 										\
	})


double bmr_feet_inches_to_meters (int feet, int inches);
double bmr_stone_lb_to_kg (int stone, int pounds);
double bmr_calc (double kg, double meters, double age, enum bmr_sex sex);
int bmr_parse_height (double *res, const char *s, const char *e);
int bmr_parse_weight (double *res, const char *s, const char *e);
