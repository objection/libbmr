#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "bmr.h"

#define $strnmatch !strncmp

#define $get_int(_res, str, e) 															\
	({ 																					\
		char *end; 																		\
		errno = 0; 																		\
		*_res = strtol (str, &end, 0); 													\
		/* If you've provided e, it's an error if you strtof doesn't */					\
		/* parse to e. If not, it's an error if you didn't parse */ 					\
		/* anything */ 																	\
		errno || (e? end != e: end == str)? 1: 0; 										\
	})

double bmr_feet_inches_to_meters (int feet, int inches) {
	inches = feet * 12 + inches;
	return inches * 0.0254;
}

double bmr_stone_lb_to_kg (int stone, int lb) {
	lb = stone * 14 + lb;
	return lb * 0.4535924;
}

// Unlike bmr_parse_{weight,height}, this doesn't take the end of the
// string. It doesn't need it. Right?
static int bmr_decompose (int *res, const char *s,
		char *delim_1, char *delim_2) {
	// I'll just void this delim_2. It's never used, but it makes it
	// clearer what this function does when you see it called.
	(void) delim_2;

	const char *p = s;
	if ($get_int (&res[0], p, 0))
		return 1;
	while (isdigit (*p))
		p++;
	if (!$strnmatch (p, delim_1, 2))
		return 1;
	p += 2;
	if ($get_int (&res[1], p, 0))
		return 1;
	// Don't bother with the rest of the string. Let you write "16st2"
	return 0;
}

// Gets meters from strings like this:
// 		5tf11in
// 		5tf11
// 		2 meters
// 		2 sheep -- if not ft/in all that matters is the number
// 		2.1
// 	s: the string
// 	e: the end of the string or 0 to parse to the 0.
int bmr_parse_height (double *res, const char *s, const char *e) {
	int feet_inches[2];
	if (bmr_decompose (feet_inches, s, "ft", "in")) {
		if ($bmr_get_double (res, s, e))
			return 1;
		return 0;
	}
	*res = bmr_feet_inches_to_meters (feet_inches[0], feet_inches[1]);
	return 0;
}

// Gets kg from strings like
// 		16st3lb
// 		16st3
// 		92kg
// 		92 sheep -- if not st/lb, all that matters is the number
// 		92.3
// 	s: the string
// 	e: the end of the string or 0 to parse to the 0.
int bmr_parse_weight (double *res, const char *s, const char *e) {
	int stone_lb[2];
	if (bmr_decompose (stone_lb, s, "st", "lb")) {
		if ($bmr_get_double (res, s, e))
			return 1;
		return 0;
	}
	*res = bmr_stone_lb_to_kg (stone_lb[0], stone_lb[1]);
	return 0;
}

// Returns your BMR, using the Miffin St Jeor equation.
double bmr_calc (double kg, double meters, double age, enum bmr_sex sex) {

	double res;

	double cm = meters * 100;
	double a = 10 * kg;
	double b = 6.25 * cm;
	double c = 5 * age;

	int s = sex == BMR_SEX_M? 5: -161;
	res = a + b - c + s;
	return res;
}

