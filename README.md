# BMR

This is a very simple library for calculating your BMR. So simple I
may well be inviting the cruel hyena-like laughter of the playground.

The important function is, of course, `bmr_calc`. It takes age, sex,
meters, and kg. Sex is an enum, `bmr_sex {BMR_SEX_F, BMR_SEX_M}`.

To get kg and meters, you can use `bmr_stone_lb_to_kg` and
`bmr_feet_inches_to_meters`.

There's also functions for parsing a strings into metric. They are
`bmr_parse_weight` and `bmr_parse_height` check bmr.c for that.
